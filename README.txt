
Taxonomy Contact - README

DESCRIPTION
-----------

Taxonomy Contact module enables to send messages to different group of users based on their taxonomy term tags in profile. Tags could be defined via vocabulary system, such as different location, interest group, gender, age group of user. 

Massive emails could be sent out via scheduled cron job within either HTML or Plain text formats. Do not require Mime Mail module for HTML format email since all emails sent via drupal_mail api. 

This version only works with one vocabulary tagged to user.

REQUIREMENTS
------------

 * Taxonomy module
 * Cron job is required if want to send out massive number of emails.

INSTALLATION
------------

 * Create a new directory "taxonomy_contact" in the sites/all/modules directory and place the entire contents of this module in it.
 * Enable the module on the Modules admin page: Administer > Site building > Modules
 * Grant the access at the Access control page: Administer > User management > Access control.
 
 * see http://drupal.org/node/70151 for further installation information.

Configuration
------------

 * Browse to admin/settings/taxonomy_contact. Select a vocabulary wants to use to classify users. Alter the number of message based on server capability. Upload a image logo for all emails.
 * Tag each user with special term under the selected vocabulary in user editing page at user/x/edit.
 * Create a new message in node/add/taxonomy-contact.
 * Run the cron if want to send out first chunk of emails immediately.

SIMILAR MODULES
------------

 * Mass Contact (http://drupal.org/project/mass_contact), mass contact classifies users by roles. Sometimes it would be hard to manage users with lots of roles in permission page. Mass contact also send out massive emails by duplicating them a number of times but not via Cron job.

